from flask import render_template, Blueprint

errors = Blueprint('errors', __name__)


@errors.app_errorhandler(403)
def error_403(error_code):
	return render_template('error_403.html', title=error_code), 403

@errors.app_errorhandler(404)
def error_404(error_code):
	return render_template('error_404.html', title=error_code), 404

@errors.app_errorhandler(500)
def error_500(error_code):
	return render_template('error_500.html', title=error_code), 500
