## From YouTube tutorial by Corey Schafer.

import os
from flaskblog import create_app

app = create_app()

if __name__ == '__main__':
	import secrets
	os.environ['SECRET_KEY'] = secrets.token_hex(16)
	app.run(debug=True)